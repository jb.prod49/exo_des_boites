import random
from math import ceil, floor
import matplotlib.pyplot as plt

#Cartons tous la meme taille TC. Boites toutes la meme taille TB.
#Si j'ai N boites, combien il me faut de carton ?
TC = 10 
TB = 2
N = 37

nb_carton = ceil((TB*N)/TC)
print("nombre de carton :",nb_carton)

#Cartons tous la meme taille TC. Boites toutes la meme taille TB.
#Si j'ai X cartons, combien il me faut de boites ?
TC = 30
TB = 3
X = 4

nb_boite = floor((TC*X)/TB)
print("nombre de boite :",nb_boite)

#Cartons tous la meme taille TC. Les boites fond entre TB et TB2.
#Si j'ai N boites, combien il me faut de carton ?
def calculer_carton(taille_carton, taille_mini_boite, taille_max_boite, nb_boite):
    liste_boites = [random.randint(taille_mini_boite, taille_max_boite) for i in range(nb_boite)]
    liste_cartons = [{'boites': [], 'taille': taille_carton}]
    for boite in liste_boites:
        for carton in liste_cartons:
            if sum(carton['boites']) + boite <= carton["taille"]:
                carton['boites'].append(boite)
                break
        else:
            new_carton = {'boites': [boite], 'taille': taille_carton}
            liste_cartons.append(new_carton)

    return len(liste_cartons)
   
TC = 12
TB = 2
TB2 = 6
N =10

nb_carton = calculer_carton(TC,TB,TB2,N)
print("nombre de carton :",nb_carton)


#Cartons tous la meme taille TC. Les boites fond entre TB et TB2.
#Si j'ai N boites et X cartons, combien je peux ranger de boites ? 
def ranger_boites(taille_carton, taille_mini_boite, taille_max_boite, nb_boites, nb_cartons):
    liste_boites = [random.randint(taille_mini_boite, taille_max_boite) for i in range(nb_boites)]
    liste_cartons = [{'boites': [], 'taille': taille_carton} for i in range(nb_cartons)]
    count = 0
    for boite in liste_boites:
        for carton in liste_cartons:
            if sum(carton['boites']) + boite <= carton["taille"]:
                carton['boites'].append(boite)
                count = count+1
                break
    return count
TC = 12
TB = 2
TB2 = 6
N = 10
X = 10
nb_carton = ranger_boites(TC,TB,TB2,N,X)
print("nombre de boites rangées :",nb_carton)

#Les cartons fond entre TC et TC2. Les boites fond entre TB et TB2.
#Si j'ai N boites et X cartons, comment je peux ranger les boites ? 
def list_boites(taille_mini,taille_max,nb_boites):
    liste_boites = [random.randint(taille_mini, taille_max) for i in range(nb_boites)]    
    return liste_boites
def list_cartons(taille_mini,taille_max,nb_cartons):
    liste_cartons = [{'boites': [], 'taille': random.randint(taille_mini, taille_max)} for i in range(nb_cartons)]
    return liste_cartons
def range_list(liste):
    return liste.sort(key=lambda x: x["taille"]-sum(x["boites"]),reverse=True)
def ranger_boitesV2(liste_cartons,liste_boites):
    range_list(liste_cartons)
    liste_boites.sort(reverse=True)
    # print("liste des boites : ",liste_boites)
    # print("liste des cartons : ",liste_cartons)
    non_range = []
    for boite in liste_boites :
        for carton in liste_cartons:
            if sum(carton['boites']) + boite <= carton["taille"] - liste_boites[-1] and sum(carton['boites']) + boite <= carton["taille"]:
                carton['boites'].extend([boite, liste_boites[-1]])
                liste_boites.pop(-1)
                # range_list(liste_cartons)
                break
            elif sum(carton['boites']) + boite <= carton["taille"]:
                carton['boites'].append(boite)
                # range_list(liste_cartons)
                break
        else :
            non_range.append(boite)
    return {"non_range":non_range,"liste_cartons":liste_cartons}

TC = 3
TC2 = 20
TB = 2
TB2 = 15
N = 10
X = 10

print("######### Teste 1 #########")
liste_des_cartons = [
    {"boites":[], "taille": 5},
    {"boites":[], "taille": 6},
    {"boites":[], "taille": 6},
    {"boites":[], "taille": 10}
]
liste_des_boites = [3, 3, 3, 3, 4, 3, 2]
print("le rangement sans random:",ranger_boitesV2(liste_des_cartons, liste_des_boites ))

print("######### Teste 2 #########")
liste_des_cartons = [
    {"boites":[], "taille": 17},
    {"boites":[], "taille": 16},
    {"boites":[], "taille": 15},
    {"boites":[], "taille": 14},
    {"boites":[], "taille": 15},
    {"boites":[], "taille": 15},
    {"boites":[], "taille": 14},
    {"boites":[], "taille": 10},
    {"boites":[], "taille": 4},
    {"boites":[], "taille": 5}
]   
liste_des_boites = [15,2,15,8,6,8,6,13,12,5]
print("le rangement sans random:",ranger_boitesV2(liste_des_cartons, liste_des_boites ))

print("######### Teste 3 #########")
liste_boites = list_boites(TB,TB2,X)
liste_cartons = list_cartons(TC,TC2,X)
rangement = ranger_boitesV2(liste_cartons,liste_boites)
print("le rangement avec des random:",rangement)

#Cartons tous la meme hauteur THC et la même TLC largeur. Boites toutes la meme hauteur THB et la meme largeur TLB.
#Si j'ai N boites, combien il me faut de carton ?
def nombre_carton(hauteur_carton, largeur_carton, hauteur_boite, largeur_boite, nombre_boites):
    carton = {
                'boites': [],
                'hauteur':hauteur_carton,
                'largeur':largeur_carton
            }
    boite = {
                'hauteur':hauteur_boite,
                'largeur':largeur_boite
            }
    liste_boites = [boite for i in range(nombre_boites)]
    
    for boite in liste_boites:
        if sum(carton['hauteur']) + boite['hauteur'] <= carton['hauteur']:
            carton['boites'].append(boite)
